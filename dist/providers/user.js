"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_1 = require("../entity/user");
exports.user = [
    {
        provide: 'UserRepository',
        useValue: user_1.User,
    },
];
//# sourceMappingURL=userProvider.js.map
