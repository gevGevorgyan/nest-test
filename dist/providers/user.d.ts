import { User } from '../entity/user';
export declare const user: {
    provide: string;
    useValue: typeof User;
}[];
