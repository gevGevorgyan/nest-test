import { Todo } from '../entity/todo';
export declare const todosProviders: {
    provide: string;
    useValue: typeof Todo;
}[];
