"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const todo_1 = require("../entity/todo");
exports.todosProviders = [
    {
        provide: 'TodosRepository',
        useValue: todo_1.Todo,
    },
];
//# sourceMappingURL=todoProvider.providers.js.map
