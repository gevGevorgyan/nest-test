"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const todos_service_1 = require("./todos.service");
const createTodo_dto_1 = require("./dto/createTodo.dto");
const swagger_1 = require("@nestjs/swagger");
let TodosController = class TodosController {
    constructor(todosService) {
        this.todosService = todosService;
    }
    async getTodos(res) {
        const todos = await this.todosService.findAll();
        return res.status(common_1.HttpStatus.OK).json(todos);
    }
    async getTodo(res, param) {
        const todos = await this.todosService.findById(param.id);
        return res.status(common_1.HttpStatus.OK).json(todos);
    }
    async createTodo(res, createTodoDTO) {
        const todo = await this.todosService.create(createTodoDTO);
        return res.status(common_1.HttpStatus.OK).json(todo);
    }
    async updateTodo(param, res, body) {
        const todo = await this.todosService.update(param.id, body);
        return res.status(common_1.HttpStatus.OK).json(todo);
    }
    async deleteTodo(param, res) {
        const todo = await this.todosService.delete(param.id);
        return res.status(common_1.HttpStatus.OK).json(todo);
    }
};
__decorate([
    common_1.Get(),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TodosController.prototype, "getTodos", null);
__decorate([
    common_1.Get('/:id'),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TodosController.prototype, "getTodo", null);
__decorate([
    common_1.Post(),
    swagger_1.ApiResponse({ status: 201, description: 'The record has been successfully created.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_a = typeof createTodo_dto_1.CreateTodoDto !== "undefined" && createTodo_dto_1.CreateTodoDto) === "function" ? _a : Object]),
    __metadata("design:returntype", Promise)
], TodosController.prototype, "createTodo", null);
__decorate([
    common_1.Patch('/:id'),
    __param(0, common_1.Param()), __param(1, common_1.Response()), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], TodosController.prototype, "updateTodo", null);
__decorate([
    common_1.Delete('/:id'),
    __param(0, common_1.Param()), __param(1, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TodosController.prototype, "deleteTodo", null);
TodosController = __decorate([
    common_1.Controller('todos'),
    __metadata("design:paramtypes", [todos_service_1.TodosService])
], TodosController);
exports.TodosController = TodosController;
//# sourceMappingURL=todoProvider.controller.js.map
