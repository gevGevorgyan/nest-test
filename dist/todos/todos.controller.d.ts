import { TodosService } from './todos.service';
import { CreateTodoDto } from './dto/createTodo.dto';
export declare class TodosController {
    private readonly todosService;
    constructor(todosService: TodosService);
    getTodos(res: any): Promise<any>;
    getTodo(res: any, param: any): Promise<any>;
    createTodo(res: any, createTodoDTO: CreateTodoDto): Promise<any>;
    updateTodo(param: any, res: any, body: any): Promise<any>;
    deleteTodo(param: any, res: any): Promise<any>;
}
