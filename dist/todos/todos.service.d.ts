import { CreateTodoDto } from './dto/createTodo.dto';
import { Todo } from '../entity/todo';
export declare class TodosService {
    private readonly todosRepository;
    constructor(todosRepository: typeof Todo);
    findAll(): Promise<Todo[]>;
    findById(ID: number): Promise<Todo>;
    create(createTodoDto: CreateTodoDto): Promise<Todo>;
    update(id: number, newValue: CreateTodoDto): Promise<Todo | null>;
    delete(ID: number): Promise<number>;
    private _assign;
}
