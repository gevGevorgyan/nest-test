"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
let TodosService = class TodosService {
    constructor(todosRepository) {
        this.todosRepository = todosRepository;
    }
    async findAll() {
        return await this.todosRepository.findAll();
    }
    async findById(ID) {
        return await this.todosRepository.findByPk(ID);
    }
    async create(createTodoDto) {
        return await this.todosRepository.create(createTodoDto);
    }
    async update(id, newValue) {
        let todo = await this.todosRepository.findByPk(id);
        if (!todo.id) {
            console.error('userProvider doesn\'t exist');
        }
        todo = this._assign(todo, newValue);
        return await todo.save({ returning: true });
    }
    async delete(ID) {
        return await this.todosRepository.destroy({
            where: { ID },
        });
    }
    _assign(todo, newValue) {
        for (const key of Object.keys(todo['dataValues'])) {
            if (todo[key] !== newValue[key]) {
                todo[key] = newValue[key];
            }
        }
        return todo;
    }
};
TodosService = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject('TodosRepository')),
    __metadata("design:paramtypes", [Object])
], TodosService);
exports.TodosService = TodosService;
//# sourceMappingURL=todoProvider.service.js.map
