import { CreateUserDto } from '../dto/createUser.dto';
export declare class UserService {
    private readonly userRepository;
    constructor(userRepository: typeof UserService);
    findAll(): Promise<UserService[]>;
    findById(ID: number): Promise<UserService>;
    create(createUserDto: CreateUserDto): Promise<UserService>;
    update(id: number, newValue: CreateUserDto): Promise<UserService | null>;
    delete(ID: number): Promise<number>;
    private _assign;
}
