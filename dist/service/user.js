"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var UserService_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
let UserService = UserService_1 = class UserService {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }
    async findAll() {
        return await this.userRepository.findAll();
    }
    async findById(ID) {
        return await this.userRepository.findByPk(ID);
    }
    async create(createUserDto) {
        return await this.userRepository.create(createUserDto);
    }
    async update(id, newValue) {
        let user = await this.userRepository.findByPk(id);
        if (!user.id) {
            console.error('userProvider doesn\'t exist');
        }
        user = this._assign(user, newValue);
        return await user.save({ returning: true });
    }
    async delete(ID) {
        return await this.userRepository.destroy({
            where: { ID },
        });
    }
    _assign(user, newValue) {
        for (const key of Object.keys(user['dataValues'])) {
            if (user[key] !== newValue[key]) {
                user[key] = newValue[key];
            }
        }
        return user;
    }
};
UserService = UserService_1 = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject('UserRepository')),
    __metadata("design:paramtypes", [Object])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=userProvider.js.map
