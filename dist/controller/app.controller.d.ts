import { AppService } from '../service/app.service';
import { AuthService } from '../service/auth.service';
export declare class AppController {
    private readonly appService;
    private readonly authService;
    constructor(appService: AppService, authService: AuthService);
    login(req: any): Promise<{
        access_token: string;
    }>;
    getHello(): string;
}
