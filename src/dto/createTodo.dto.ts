
export class CreateTodoDto {
    readonly id: number;

    readonly text: string;

    readonly complete: boolean;
}
