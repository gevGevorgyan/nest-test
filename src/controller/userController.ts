import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { UserService } from '../service/userService';
import { CreateUserDto } from '../dto/createUser.dto';
import { ApiResponse } from '@nestjs/swagger';

@Controller('users')
export class UserController {
    constructor(private readonly userService: UserService) { }

    @Get()
    public async getUsers(@Response() res) {
        const user = await this.userService.findAll();
        return res.status(HttpStatus.OK).json(user);
    }

    @Get('/:id')
    public async getUser(@Response() res, @Param() param) {
        const user = await this.userService.findById(param.id);
        return res.status(HttpStatus.OK).json(user);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createUser(@Response() res, @Body() createUserDTO: CreateUserDto) {

        const user = await this.userService.create(createUserDTO);
        return res.status(HttpStatus.OK).json(user);
    }

    @Patch('/:id')
    public async updateUser(@Param() param, @Response() res, @Body() body) {

        const user = await this.userService.update(param.id, body);
        return res.status(HttpStatus.OK).json(user);
    }

    @Delete('/:id')
    public async deleteUser(@Param() param, @Response() res) {

        const user = await this.userService.delete(param.id);
        return res.status(HttpStatus.OK).json(user);
    }
}
