import {Injectable, Inject} from '@nestjs/common';
import {CreateUserDto} from '../dto/createUser.dto';
import {User} from '../entity/user';

@Injectable()
export class UserService {
    password: string;
    constructor(
        @Inject('UserRepository') private readonly userRepository: typeof User,
    ) {
    }

    async findAll(): Promise<UserService[]> {
        return await this.userRepository.findAll<User>();
    }

    async findById(ID: string): Promise<UserService> {
        return this.userRepository.findByPk(ID);
    }

    async findOne(username): Promise<UserService> {
        return this.userRepository.findOne(username);
    }

    async create(createUserDto: CreateUserDto): Promise<UserService> {
        return await this.userRepository.create<User>(createUserDto);
    }

    async update(id: string, newValue: CreateUserDto): Promise<UserService | null> {

        let user = await this.userRepository.findByPk<User>(id);

        if (!user.id) {
            // tslint:disable-next-line:no-console
            console.error('userProvider doesn\'t exist');
        }

        user = this._assign(user, newValue);

        return await user.save({returning: true});
    }

    public async delete(ID: string): Promise<string> {

        return await this.userRepository.destroy({
            where: {ID},
        });
    }

    private _assign(user: CreateUserDto, newValue: CreateUserDto): User {
        // tslint:disable-next-line:no-string-literal
        for (const key of Object.keys(user['dataValues'])) {
            if (user[key] !== newValue[key]) {
                //
                user[key] = newValue[key];
            }
        }
        return user as unknown as User;
    }
}
