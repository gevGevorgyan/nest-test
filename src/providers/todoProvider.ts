import { Todo } from '../entity/todo';

export const todoProvider = [
    {
        provide: 'TodosRepository',
        useValue: Todo,
    },
];
