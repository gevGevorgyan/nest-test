import { User } from '../entity/user';

export const userProvider = [
    {
        provide: 'UserRepository',
        useValue: User,
    },
];
