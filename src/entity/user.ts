import {Table, Column, Model, DataType, CreatedAt, UpdatedAt} from 'sequelize-typescript';

@Table({timestamps: true})
export class User extends Model<User> {
    @Column({
        defaultValue: DataType.UUIDV4,
        type: DataType.UUID,
        primaryKey: true,
        unique: true,
        field: 'ID',
    })
    id: string;

    @Column
    firstName: string;

    @Column
    lastName: string;

    @Column
    email: string;

    @Column
    password: string;

    @Column
    avatar: string;

    @Column
    cover: string;

    @CreatedAt
    creationDate: Date;

    @UpdatedAt
    updatedOn: Date;
}
