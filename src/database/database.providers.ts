import { Sequelize } from 'sequelize-typescript';
import { Todo } from '../entity/todo';
import { User } from '../entity/user';

export const databaseProviders = [
    {
        provide: 'SequelizeToken',
        useFactory: async () => {
            const sequelize = new Sequelize({
                dialect: 'postgres',
                host: 'localhost',
                port: 5432,
                username: 'postgres',
                password: 'postgres',
                database: 'testDB',
            });
            sequelize.addModels([Todo, User]);
            await sequelize.sync();
            return sequelize;
        },
    },
];
