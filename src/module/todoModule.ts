import { Module } from '@nestjs/common';
import { TodoController } from '../controller/todoController';

import { DatabaseModule } from '../database/database.module';
import { TodoService } from '../service/todoService';
import { todoProvider } from '../providers/todoProvider';
@Module({
  imports: [DatabaseModule],
  controllers: [TodoController],
  providers: [TodoService, ...todoProvider ],
})
export class TodoModule {}
