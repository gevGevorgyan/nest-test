import { Module } from '@nestjs/common';

import { databaseProviders } from '../database/database.providers';
import { AppController } from '../controller/app.controller';
import { DatabaseModule } from '../database/database.module';
import { AppService } from '../service/app.service';
import { TodoModule } from './todoModule';
import { UserModule } from './userModule';
import {AuthModule} from './auth.module';

@Module({
  imports: [DatabaseModule, TodoModule, UserModule, AuthModule],
  controllers: [AppController],
  providers: [AppService, ...databaseProviders],
})
export class AppModule {}
