import {Module} from '@nestjs/common';

import {UserModule} from './userModule';

import {AuthService} from '../service/auth.service';
import {PassportModule} from '@nestjs/passport';
import {LocalStrategy} from '../local.strategy';
import {JwtStrategy} from '../jwt.strategy';
import {JwtModule} from '@nestjs/jwt';
import {UserService} from '../service/userService';
import {DatabaseModule} from '../database/database.module';
import {userProvider} from '../providers/userProvider';

@Module({
    imports: [
        userProvider[0].useValue,
        UserModule,
        PassportModule.register({
            defaultStrategy: 'jwt',
            property: 'user',
            session: false,
        }),
        JwtModule.register({
            secret: 'jwtConstants.secret',
            signOptions: {expiresIn: '7d'},
        }),
    ],
    providers: [AuthService, LocalStrategy, JwtStrategy, UserService],
    exports: [PassportModule, LocalStrategy, AuthService],
})
export class AuthModule {
}
