import { Module } from '@nestjs/common';
import { UserController } from '../controller/userController';

import { DatabaseModule } from '../database/database.module';
import { UserService } from '../service/userService';
import { userProvider } from '../providers/userProvider';
// import {userRepository} from './../providers/userProvider';
@Module({
  imports: [DatabaseModule, userProvider[0].useValue],
  controllers: [UserController],
  providers: [UserService, ...userProvider ],
})
export class UserModule {}
