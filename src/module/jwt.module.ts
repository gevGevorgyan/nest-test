import {JwtModule} from '@nestjs/jwt';
import { Module } from '@nestjs/common';
import {AppService} from '../service/app.service';

@Module({
    imports: [JwtModule.register({ secret: 'hard!to-guess_secret' })],
    providers: [AppService],
})

export class AuthModule {}
