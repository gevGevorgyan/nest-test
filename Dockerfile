# Images from which app will built
FROM node:latest

# Create project directory
WORKDIR /usr/src/nest-test

# Install dependencies
COPY package.json ./
RUN yarn install

# Bind project source
COPY . .

# Expose port
EXPOSE $PORT

# Command to run app
CMD [ "yarn", "start" ]
